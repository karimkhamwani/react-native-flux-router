import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View
} from 'react-native';
import { Router, Scene, Drawer, Stack } from 'react-native-router-flux';
import { connect, Provider } from 'react-redux';
import configureStore from './store/configureStore';
const store = configureStore()
const RouterWithRedux = connect()(Router);
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

import Landing from './components/landing';
import PageTwo from './components/pageTwo';
import Home from './components/home';
import Search from './components/search';
import Login from './components/login';
import Detail from './components/detail';

const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{ color: selected ? 'red' : 'black' }}>{title}</Text>
  )
}

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Scene key="root">
            <Scene key="drawer" drawer
              contentComponent={Landing}
              drawerWidth={260}
              initial
              hideNavBar
              navigationBarStyle={{ backgroundColor: '#16334c' }}
              titleStyle={{ color: '#fff', alignSelf: 'center', marginRight: 10, textAlign: 'center' }}
            >
              <Stack
                key="root"
                transitionConfig={() => ({ screenInterpolator: CardStackStyleInterpolator.forHorizontal })}>
                <Scene
                  key="dashboard"
                  component={Landing}
                  title="Dashboard"
                  hideNavBar
                  initial
                />
                <Scene
                  key="home"
                  component={Home}
                  title="Dashboard"
                  hideNavBar
                  direction="leftToRight"
                />
              </Stack>

            </Scene>
          </Scene>
        </Router>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  tarBarStyle: {
    backgroundColor: '#FFFFFF',
    opacity: 0.5,
  }
})


  /*< Router >
  <Scene overlay>
    <Scene key="lightbox" lightbox leftButtonTextStyle={{ color: 'green' }} backButtonTextStyle={{ color: 'red' }} initial>
      <Scene key="modal" modal hideNavBar>
        <Scene key="auth" initial>
          <Scene key="login" component={Login} hideNavBar />
        </Scene>
        <Scene key="drawer" drawer
          contentComponent={SideMenu}
          drawerIcon={dr}
          drawerWidth={260}
          navigationBarStyle={{ backgroundColor: '#16334c' }}
          titleStyle={{ color: '#fff', alignSelf: 'center', marginRight: padd, textAlign: 'center' }}
        >
          <Scene key="main" >
            <Scene
              key="dashboard"
              component={Dashboard}
              title="Dashboard"
            />

            <Scene
              key="complete"
              component={CompleteWork}
              title="Complete Work Order"
              back={true}
              backButtonTintColor="white"
            />
            <Scene
              key="storage"
              component={FileStorage}
              title="Storage Center"
              back={true}
              backButtonTintColor="white"
            />
            <Scene
              key="logout"
              component={Logout}
              title="Logout"
              modal
              hideNavBar
              drawer={false}

            />

          </Scene>
        </Scene>
      </Scene>
    </Scene>
  </Scene>

</Router >*/